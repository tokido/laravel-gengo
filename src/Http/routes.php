<?php

/*
|--------------------------------------------------------------------------
| Translations Routes
|--------------------------------------------------------------------------
|
*/

\Route::group(['domain' => config('dikateny.domain.callback')], function ($router) {

    $router->post('callback/translations', [
        'as'    => 'gengo.translations.callback',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyCallbackController@callback'
    ]);
});

\Route::group(['domain' => config('dikateny.domain.drequest')], function ($router) {
    
    $router->get('dikateny/order/cancel/{id}', [
        'as'    => 'dikateny.translations.cancel.order',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@cancelAvailableTranslationRequestByOrderId'
    ]);

    $router->get('dikateny/order/revise/{id}/{back_route?}', [
        'as'    => 'dikateny.translations.revise.order',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@requestRevisionByOrderId'
    ]);

    $router->get('dikateny/order/reject/{id}/{back_route?}', [
        'as'    => 'dikateny.translations.reject.order',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@requestRejectionByOrderId'
    ]);

    $router->get('dikateny/order/approval/{id}/{applytranslation}/{back_route?}', [
        'as'    => 'dikateny.translations.approval.order',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@requestApprovalJobsById'
    ]);

    $router->get('dikateny/repsonse/preview/{id}/{back_route?}', [
        'as'    => 'dikateny.translations.preview.response',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@previewResponseById'
    ]);


    $router->post('dikateny/order/submit/revision', [
        'as'    => 'dikateny.translations.revise.submit',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@submitRevisionByOrderId'
    ]);

    $router->post('dikateny/order/submit/reject', [
        'as'    => 'dikateny.translations.reject.submit',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@rejectRevisionByOrderId'
    ]);

    $router->post('dikateny/order/submit/approval', [
        'as'    => 'dikateny.translations.approval.submit',
        'uses'  => 'Tokido\Dikateny\Controllers\DikatenyRequestController@approveRevisionByOrderId'
    ]);

});
