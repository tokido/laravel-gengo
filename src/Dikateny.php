<?php

namespace Tokido\Dikateny;

use App;
use Gengo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\Log;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;
use Tokido\Dikateny\Model\TranslationRequestJobRevision;


class Dikateny {

    public $secretKey;
    public $apiKey;
    public $apiJobs;

    public function __construct(
       $apiKey,
       $secretKey
    ){
        \Gengo\Config::setApiKey($apiKey);
        \Gengo\Config::setPrivateKey($secretKey);
        $this->apiKey    = $apiKey;
        $this->secretKey = $secretKey;
        $this->apiJobs   = new \Gengo\Jobs();
        $this->apiJob    = new \Gengo\Job();
        $this->apiOrders = new \Gengo\Order();
    }


    /*
    * return $Job
    * Get comments are not implemented cause comments is instantly saved 
    * feedback ? need to get feedback or not  ?
    * next: reviseJob
    */
    public function postJobs($jobs)
    { 
        $this->apiJobs->postJobs($jobs);
        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function getJobs($status = null, $timestampafter = null, $count = null) 
    {
        $this->apiJobs->getJobs($status, $timestampafter, $count); 

        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function getJobsByIds(array $ids)
    {
        $this->apiJobs->getJobsByID($ids);
        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function getSingleJob($id)
    {
        $this->apiJob->getJob($id);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    /*use reviseJobs if you want to revise multiple jobs with single comment*/
    public function reviseJobs(array $jobs, $comment = null)
    {
        $this->apiJobs->revise($jobs, $comment);

        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    /*use reviseJobs if you want to revise single job with a comment*/
    public function reviseJob($jobId, $comment = null)
    {
        $this->apiJob->revise($jobId, $comment);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function approveJob($jobId, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {
        $args = array('rating' => $rating, 'public' => $public);

        if($for_translator != null)
            $args['for_translator'] = $for_translator;

        if($for_mygengo != null)
            $args['for_mygengo'] = $for_mygengo;

        $this->apiJob->approve($jobId, $args);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function buildJobApprovalArgs($jobId, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {

        $args = array('job_id' => $jobId ,'rating' => $rating, 'public' => $public);

        if($for_translator != null)
            $args['for_translator'] = $for_translator;

        if($for_mygengo != null)
            $args['for_mygengo'] = $for_mygengo;

        return $args;
    }

    public function approveMultipleJobs(array $jobs) 
    {
        $this->apiJobs->approve($jobs);
        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function cancelJobByID($jobID) 
    {

        $this->apiJob->cancel($jobID);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function cancelOrderByID($orderID) 
    {
        $this->apiOrders->cancel($orderID);

        return json_decode($this->apiOrders->getResponseBody(), true);
    }

    public function archiveSingleJob($jobId)
    {
        $this->apiJob->archive($jobId);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function archiveMultipleJobs(array $jobIds) 
    {
        $this->apiJobs->archive($jobIds);

        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function rejectSingleJob($jobId,$comment, $captcha, $follow_up = "requeue", $reason = "quality")
    {
        $args = array('comment' => $comment, 'captcha' => $captcha, 'follow_up' => $follow_up, 'reason' => $reason);

        $this->apiJob->reject($jobId, $args);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function builtRejectJobRequests($jobId , $comment, $captcha, $follow_up = "requeue", $reason = "quality")
    {
        return $job =  array('job_id' => $jobId, 'comment' => $comment , 'captcha' => $captcha, 'follow_up' => $follow_up, 'reason' => $reason);
    }

    public function rejectMultipleJob($jobs, $comment)
    {

        $this->apiJobs->reject($jobs, $comment);

        return json_decode($this->apiJobs->getResponseBody(), true);
    }

    public function commentOrder($orderId, $body)
    {
        $this->apiOrders->postComment($orderId, $body);
        
        return json_decode($this->apiOrders->getResponseBody(), true);
    }

    public function commentJob($jobId, $body)
    {
        $this->apiJob->postComment($jobId, $body);

        return json_decode($this->apiJob->getResponseBody(), true);
    }

    public function createJob(
        $slug , $body_src, $type = "text", $tier = "standard", $force = 1, $lc_src = "fr", $lc_tgt = "en"
    ){

        return $job = array(
            "type"     => $type,
            "slug"     => $slug,
            "body_src" => $body_src,
            "lc_src"   => $lc_src,
            "lc_tgt"   => $lc_tgt,
            "tier"     => $tier,
            "force"    => $force,
        );
    }

    public function createUpdatableJob(
        $job_id, $slug, $body_src, $lc_src = "fr", $lc_tgt = "en",  $tier = "standard", $status = "available"
    ){
        return $job = array(
            'job_id'    => $job_id,
            'slug'      => $slug,
            'body_src'  => $body_src,
            'lc_src'    => $lc_src,
            'lc_tgt'    => $lc_tgt,
            'tier'      => $tier,
            'status'    => $status
        );
    }

    public function createJobList(array $jobList)
    {
        $jobs = array();

        foreach ($jobList as $key => $job) {
            $iteration = $key +1;
            $jobs['job_' . $iteration] = $job;
        }
        return $jobs;
    }

    /*get Order by job*/
    public function getOrder($orderId)
    {
        return json_decode($this->apiOrders->getOrder($orderId),  true);
    }

    public function getAllJobIdsAndStatusFromOrder($orders, $status = null)
    {
        $jobLists = array();
        if($status == null) {
            $jobstatusList = self::getJobsStatusList();    
        } else {
            $jobstatusList = $status;
        }
        
        if($orders && $orders['opstat'] && $orders['opstat'] == 'ok' && sizeof($orders['response']) > 0) {
            foreach ($orders['response'] as $order) {
                foreach ($order as $key => $value) {
                    if(in_array($key, $jobstatusList)) {
                        $jobLists =  array_merge($jobLists, $value);
                    }
                }
            }
        }

        return $jobLists;
    }

    public function rejectMultipleJobs($jobsRejectRequests, $comment)
    {
        $requests = array();

        foreach ($jobsRejectRequests as $jobsRejectRequest) {
            extract($jobsRejectRequest);
            array_push($requests, $this->builtRejectJobRequests($jobId, $comment, $captcha));
        }

        return $this->rejectMultipleJob($requests, $comment);
    }  

    public function reviseMultipleJobByIds(array $jobIds, $body)
    {
        $responses = array();
        $response = $this->reviseJobs($jobIds, $body);
        /*check for each response in array*/
        if($response && is_array($response)) {
            foreach ($jobIds as $job) {
                $revisionData = $this->saveRevisionsComment($job['job_id'], $body);
                $job = TranslationRequestJob::where('job_id', $job['job_id'])->first();

                if($job) {
                    $job->status = 'revising';
                    $job->translationRevisions()->save($revisionData);
                    array_push($responses, $revisionData);
                    $order = $job->order;
                    $order->status = 'revising';
                    $order->save();
                    $job->save();
                }
            }
        }
        return $responses;
    }

    public function saveRevisionsComment($job_id, $comment)
    {
        $revision = new TranslationRequestJobRevision(
            [
                'job_id'    => $job_id,
                'comment'   => $comment
            ]
        );
        return $revision;
    }

    public function approveMultipleJobsWithSameNote($jobIds, $rating = 3 , $for_translator = 'nice', $for_mygengo = 'no note', $public = false)
    {
        $approvals = array();
        foreach ($jobIds as $jobId) {
            $approval = $this->buildJobApprovalArgs($jobId, $rating, $for_translator, $for_mygengo, $public);
            /*save approved Jobs*/
            $savedJob = TranslationRequestJob::where('job_id', $jobId)->first();
            $savedJob->status = 'approved';
            $savedJob->save();
            $response = $savedJob->callbackResponses()->orderBy('created_at', 'desc')->first();
            $response->status = 'approved';
            $response->save();
            $order = $savedJob->order;
            $order->status = 'approved';
            $order->save();

            array_push($approvals, $approval);
        }

        return $this->approveMultipleJobs($approvals);
    }


    public static function getJobsStatusList()
    {

        return array(
            "jobs_available",
            "jobs_pending",
            "jobs_reviewable",
            "jobs_approved",
            "jobs_revising",
            "jobs_cancelled",
            "jobs_held"
        );
    }
}
