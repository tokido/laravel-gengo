<?php

namespace Tokido\Dikateny;

use Illuminate\Support\ServiceProvider;

class DikatenyServiceProvider extends ServiceProvider
{
    /**
     * Register the boot
     */
    public function boot()
    {
        $modulePath = realpath(__DIR__);
        
        $config_path = $modulePath . '/config/dikateny.php';

        $this->mergeConfigFrom($config_path, 'dikateny');

        $this->publishes([
            $config_path => config_path('dikateny.php')
        ]);

        $this->loadRoutesFrom( $modulePath . '/Http/routes.php');

        $this->loadMigrationsFrom(__DIR__ . '/migrations/');

        $this->loadViewsFrom(__DIR__.'/views', 'dikateny');
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //dd(config('dikateny.api'));
        $this->app->singleton(Dikateny::class, function () {
            return new Dikateny(
                config('dikateny.api.key'),
                config('dikateny.api.privatekey')
            );
        });

        $this->app->alias(Dikateny::class, 'dikateny');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Dikateny::class];
    }
}
