<form method="post" action="{{route('dikateny.translations.reject.submit')}}">
    
    <input type="hidden" name="back_route" value="{{$back_route}}">
    @if($responses)
        @foreach($responses as $key => $jobResponse)
            <label>Commentaire pour {{$key}}</label>
            <input type="text" name="comments[{{$jobResponse->job_id}}]" ><br>
            {{$key}}: {!!$jobResponse->body_tgt!!}<br>

            <label>Captcha pour {{$key}}</label>
            <input type="text" name="captcha[{{$jobResponse->job_id}}]" ><br>
            <img src="{{$jobResponse->captcha_url}}" alt="{{$jobResponse->captcha_url}}"></img>
            <br>
        @endforeach
    @endif
    <br><br><br>


    <label>Commentaire global</label>
    <input type="text" name="global_comment" ><br>

    <input type="submit" value="Submit">
    <a href="{{$back_route ? base64_decode($back_route) : '#'}}">Annuler </a>
</form> 
