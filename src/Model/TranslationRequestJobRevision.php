<?php 

namespace Tokido\Dikateny\Model;

Use Illuminate\Database\Eloquent\Model;

class TranslationRequestJobRevision extends Model
{

    protected $fillable = [
        'job_id', 
        'translation_request_job_id', 
        'comment'
    ];

    public function job()
    {
        return $this->belongsTo('Tokido\Dikateny\Model\TranslationRequestJob', 'translation_request_job_id');
    }
}