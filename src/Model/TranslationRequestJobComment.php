<?php 

namespace Tokido\Dikateny\Model;

Use Illuminate\Database\Eloquent\Model;

class TranslationRequestJobComment extends Model
{

    protected $fillable = [
        'job_id', 
        'body', 
        'author', 
        'ctime',
        'order_id'
    ];

    public function job()
    {
        return $this->belongsTo('Tokido\Dikateny\Model\TranslationRequestJob', 'translation_request_job_id');
    }
}