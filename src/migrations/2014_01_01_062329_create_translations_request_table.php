<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationsRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_request_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('wait_validation')->nullable()->default(false);
            $table->string('status')->default('queued');
            $table->string('order_id'); 
            $table->integer('job_count');
            $table->string('credits_used')->nullable();
            $table->string('currency')->nullable();
            $table->integer('translatable_id');
            $table->string('translatable_type');
            $table->integer('translation_force');
            $table->timestamps();
        });

        Schema::create('translation_request_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('translation_request_order_id')->unsigned()->index('ndx_translation_request_order_id')->nullable();
            $table->foreign('translation_request_order_id', 'fk_translation_request_order_id')->references('id')->on('translation_request_orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->integer('job_id');
            $table->string('slug');  // Correspondance dans le schema : 
            $table->string('order_id');
            $table->text('body_src');
            $table->string('lc_src');   
            $table->string('lc_tgt');  
            $table->string('unit_count');   
            $table->string('tier');
            $table->string('credits'); 
            $table->string('currency');
            $table->string('status');
            $table->integer('eta');
            $table->integer('ctime');   
            $table->string('auto_approve'); 
            $table->string('position');   
            $table->string('callback_url'); 
        });

        DB::statement("ALTER TABLE `translation_request_jobs` CHANGE `body_src` `body_src` MEDIUMBLOB  NULL");

        Schema::create('translation_request_job_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('translation_request_job_id')->unsigned()->index('ndx_translation_request_job_id')->nullable();
            $table->foreign('translation_request_job_id', 'fk_translation_request_job_id')->references('id')->on('translation_request_jobs')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->string('job_id');
            $table->text('body_src'); 
            $table->string('lc_src'); 
            $table->string('lc_tgt'); 
            $table->string('unit_count'); 
            $table->string('tier'); 
            $table->string('credits'); 
            $table->string('status'); 
            $table->string('eta'); 
            $table->string('ctime'); 
            $table->string('order_id'); 
            $table->string('callback_url'); 
            $table->string('auto_approve'); 
            $table->string('preview_url'); 
            $table->string('captcha_url'); 
            $table->text('body_tgt'); 
            $table->timestamps();
            
        });

        DB::statement("ALTER TABLE `translation_request_job_responses` CHANGE `body_src` `body_src` MEDIUMBLOB  NULL");

        DB::statement("ALTER TABLE `translation_request_job_responses` CHANGE `body_tgt` `body_tgt` MEDIUMBLOB  NULL");

        Schema::create('translation_request_job_comments', function (Blueprint $table) {
            $table->increments('id');
            /*$table->integer('translation_request_job_id')->unsigned()->index('ndx_translation_request_job_cmt_id')->nullable();
            $table->foreign('translation_request_job_id', 'fk_translation_request_job_cmt_id')->references('id')->on('translation_request_jobs')->onUpdate('NO ACTION')->onDelete('CASCADE');*/
            $table->integer('commentable_id');
            $table->string('commentable_type');
            $table->string('job_id')->nullable();
            $table->string('order_id')->nullable();
            $table->string('author');
            $table->string('ctime')->nullable();
            $table->text('body');  
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `translation_request_job_comments` CHANGE `body` `body` MEDIUMBLOB  NULL");

        Schema::create('translation_request_job_revisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('translation_request_job_id')->unsigned()->index('ndx_translation_request_job_rvs_id')->nullable();
            $table->foreign('translation_request_job_id', 'fk_translation_request_job_rvs_id')->references('id')->on('translation_request_jobs')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->text('comment');  
            $table->string('job_id')->nullable();
            $table->timestamps();
        });

         DB::statement("ALTER TABLE `translation_request_job_revisions` CHANGE `comment` `comment` MEDIUMBLOB  NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_request_job_revisions');
        Schema::dropIfExists('translation_request_job_comments');
        Schema::dropIfExists('translation_request_job_responses');
        Schema::dropIfExists('translation_request_jobs');
        Schema::dropIfExists('translation_request_orders');
    }
}
